`timescale 1ns / 1ps

module test;
    reg x1_in,x2_in,x3_in,x4_in,x5_in,x6_in,x7_in,x8_in;
    wire y1_out,y2_out,y3_out,y4_out,y5_out,y6_out,y7_out;
    
    main main_test(
                    .x1(x1_in),
                    .x2(x2_in),
                    .x3(x3_in),
                    .x4(x4_in),
                    .x5(x5_in),
                    .x6(x6_in),
                    .x7(x7_in),
                    .x8(x8_in),
                    
                    .y1(y1_out),
                    .y2(y2_out),
                    .y3(y3_out),
                    .y4(y4_out),
                    .y5(y5_out),
                    .y6(y6_out),
                    .y7(y7_out)
                    );
     integer i1, i2;
     reg [31:0] bcd_1;
     reg [31:0] bcd_2;
     reg [31:0] expected_val;
     
     initial begin
     bcd_1 = 0;
     bcd_2 = 0;
     
        for (i2 = 0; i2 < 10; i2 = i2 + 1) begin
            
            bcd_2 = i2;
            for (i1 = 0; i1 < 10; i1 = i1 + 1) begin
                
                bcd_1 = i1;
                x1_in = bcd_1[0];
                x2_in = bcd_1[1];
                x3_in = bcd_1[2];
                x4_in = bcd_1[3];
            
                x5_in = bcd_2[0];
                x6_in = bcd_2[1];
                x7_in = bcd_2[2];
                x8_in = bcd_2[3];
            
                
                expected_val = i2 * 10 + i1;
                #10
                if (y1_out == expected_val[0] & y2_out == expected_val[1] & y3_out == expected_val[2] &
                    y4_out == expected_val[3] & y5_out == expected_val[4] & y6_out == expected_val[5] &
                    y7_out == expected_val[6]) begin
                    $display("Output is correct!");
                end else begin
                    
                    $display("The output is wrong");
                    
                    // x0=%b, x1=%b, x2=%b, x3=%b, x4=%b, x5=%b, x6=%b, x7=%b," +
                    //"y0=%b, y1=%b, y2=%b, y3=%b, y4=%b, y5=%b, y6=%b,   expected=%b,%b,%b",
                    $display(x1_in,x2_in,x3_in,x4_in,x5_in,x6_in,x7_in,x8_in);
                    $display(y1_out,y2_out,y3_out,y4_out,y5_out,y6_out,y7_out);
                    $display(expected_val[0], expected_val[1], expected_val[2], expected_val[3], expected_val[4],
                    expected_val[5], expected_val[6]);
                    
                end
                bcd_1 = 0;
            end
            bcd_2 = 0;
        end
        #10 $stop;
    end
endmodule