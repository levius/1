`timescale 1ns / 1ps

module pr6(
    input x1,
    input x2,
    input x3,
    input x4,
    input x5,

    output y1,
    output y2,
    output y3,    
    output y4,
    output y5   
    );
    
    wire nx1, nx2, nx3, nx4, nx5;
    nand(nx1,x1,x1);
    nand(nx2,x2,x2);
    nand(nx3,x3,x3);
    nand(nx4,x4,x4);
    nand(nx5,x5,x5);
    
    //2 output
    wire nx2x5,x2nx5;
    and(nx2x5,nx2,x5);
    and(x2nx5,x2,nx5);
    or(y1,nx2x5,x2nx5);
    
    //4 output
    wire nx1_nx2_x4, nx1_x2_nx4_x5, nx1_x4_nx5, 
    x1_nx2_nx4, x1_nx4_nx5, x1_x2_x4_x5;
    and(nx1_nx2_x4,nx1,nx2,x4);
    and(nx1_x2_nx4_x5,nx1,x2,nx4,x5);
    and(nx1_x4_nx5,nx1,x4,nx5);
    and(x1_nx2_nx4,x1,nx2,nx4);
    and(x1_nx4_nx5,x1,nx4,nx5);
    and(x1_x2_x4_x5,x1,x2,x4,x5);
    or(y2,nx1_nx2_x4, nx1_x2_nx4_x5, nx1_x4_nx5, 
    x1_nx2_nx4, x1_nx4_nx5, x1_x2_x4_x5);
    
    //8 output
    wire nx2_x3_nx4_nx5, nx1_x2_nx3_nx4, nx1_x2_nx3_nx5,
     x1_nx2_nx3_x4, x2_nx3_nx4_nx5;
    and(nx2_x3_nx4_nx5,nx2,x3,nx4,nx5);
    and(nx1_x2_nx3_nx4,nx1,x2,nx3,nx4);
    and(nx1_x2_nx3_nx5,nx1,x2,nx3,nx5);
    and(x1_nx2_nx3_x4,x1,nx2,nx3,x4);
    and(x2_nx3_nx4_nx5,x2,nx3,nx4,nx5);
    or(y3,nx2_x3_nx4_nx5, nx1_x2_nx3_nx4, nx1_x2_nx3_nx5,
     x1_nx2_nx3_x4, x2_nx3_nx4_nx5);
     
    //16 output
    wire nx1_x2_x4_x5, nx1_x2_x3, x1_nx2, x1_x2_nx3_nx4_nx5;
    and(nx1_x2_x4_x5,nx1,x2,x4,x5);
    and(nx1_x2_x3,nx1,x2,x3);
    and(x1_nx2,x1,nx2);
    and(x1_x2_nx3_nx4_nx5,x1,x2,nx3,nx4,nx5);
    or(y4,nx1_x2_x4_x5, nx1_x2_x3, x1_nx2, x1_x2_nx3_nx4_nx5);
    
    //32 output
    wire x1_x2_x5, x1_x2_x4,x1_x2_x3;
    and(x1_x2_x5,x1,x2,x5);
    and(x1_x2_x4,x1,x2,x4);
    and(x1_x2_x3,x1,x2,x3);
    or(y5,x1_x2_x5, x1_x2_x4,x1_x2_x3);
    
endmodule




module main(
    input x1,
    input x2,
    input x3,
    input x4,
    input x5,
    input x6,
    input x7,
    input x8,
    output y1,
    output y2,
    output y3,
    output y4,
    output y5,
    output y6,
    output y7);
    
    wire PR1_2,PR1_4,PR1_8,PR1_16,PR1_32;
    pr6 PR1(x6,x5,x4,x3,x2,PR1_2,PR1_4,PR1_8,PR1_16,PR1_32);
    wire unused_output;
    pr6 PR2(x8, x7, PR1_32, PR1_16, PR1_8,y4,y5,y6,y7,unused_output);
    or(y1,x1);
    or(y2,PR1_2);
    or(y3,PR1_4);
    
endmodule


